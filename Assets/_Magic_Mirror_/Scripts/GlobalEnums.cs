﻿using UnityEngine;
using System.Collections;

    public class GlobalEnums
    {
        public enum AutoHeightAngle : int { DontUse, ShowInfoOnly, AutoUpdate, AutoUpdateAndShowInfo }
        public enum UserMapType : int { None, RawUserDepth, BodyTexture, UserTexture, CutOutTexture }
    }